import React, { Component, lazy, Suspense } from "react";
import "./App.css";
import { ConnectedRouter } from "connected-react-router";
import { Switch } from "react-router-dom";
import Route from "./components/Route";
import ReduxToastr from "react-redux-toastr";
import Loader from "./components/Loader/Loader";
import "./scss/style.scss";

const Home = lazy(() => import("./pages/Home"));

class App extends Component {
  render() {
    return (
      <ConnectedRouter history={this.props.history}>
        <Suspense fallback={<Loader active={true} />}>
          <Switch>
            <Route path="/" name="Home" component={Home} />
          </Switch>
          <ReduxToastr />
        </Suspense>
      </ConnectedRouter>
    );
  }
}

export default App;
