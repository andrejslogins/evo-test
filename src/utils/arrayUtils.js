// Find the index of the next larger value in array
export const findPreviousIndex = (value, array) => {
  // If larger, set to infinity
  let indexArr = array.map(k => {
    let diff = value - k;
    if (diff < 0) return Infinity;
    return diff;
  });
  let min = Math.min.apply(Math, indexArr);
  // If an empty array is given or only larger values are supplied, return -1
  if (min === Infinity) return -1;
  return indexArr.indexOf(min);
};
