import { findPreviousIndex } from "../arrayUtils";

describe("findPreviousIndex", () => {
  it("should get first value", () => {
    expect(findPreviousIndex(12, [1, 15, 17])).toEqual(0);
  });

  it("should return -1", () => {
    expect(findPreviousIndex(12, [])).toEqual(-1);
  });

  it("should return 3", () => {
    expect(findPreviousIndex(7, [1, 3, 5, 6, 8, 9, 13])).toEqual(3);
  });
});
