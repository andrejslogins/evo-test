import React, { useRef } from "react";
import TableList from "../../components/TableList";

const Home = ({ showTables }) => {
  const tableListRef = useRef(null);
  let width = 0;
  if (tableListRef.current) {
    width = tableListRef.current.clientWidth;
  } else {
    width = 0;
  }
  return (
    <div ref={tableListRef}>{showTables && <TableList width={width} />}</div>
  );
};

export default Home;
