import Home from "./Home";
import {
  subscribeToTables,
  unsubscribeFromTables,
} from "../../store/actions/appActions";
import { connect } from "react-redux";

const mapStateToProps = ({
  app: {
    tables: { showTables },
  },
}) => ({
  showTables,
  //Insert redux state parameters if necessarry
});

const mapDispatchToProps = {
  // Insert actions here
  subscribeToTables,
  unsubscribeFromTables,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
