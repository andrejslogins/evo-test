import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import * as serviceWorker from "./serviceWorker";
import enLocaleData from "react-intl/locale-data/en";
import { createBrowserHistory } from "history";
import { IntlProvider } from "react-intl-redux";
import { addLocaleData } from "react-intl";
import localeData from "./assets/messages/index";
import setupSocket from "./store/sockets";

const history = createBrowserHistory();
const initialState = {
  intl: {
    locale: "en",
    messages: localeData.en,
  },
};

const socket = setupSocket();
export const store = configureStore(initialState, history, socket);

addLocaleData([...enLocaleData]);

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider
      key={initialState.locale}
      locale={initialState.locale}
      messages={initialState.messages}
    >
      <App history={history} />
    </IntlProvider>
  </Provider>,
  document.getElementById("root"),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
