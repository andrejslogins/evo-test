export default {
  "data.fetch": "Fetch data",
  "api.fetch.error": "A problem has occured",
  submit: "Submit",
  username: "Username",
  password: "Password",
  participants: "Participants",
  name: "Name",
  ok: "OK",
};
