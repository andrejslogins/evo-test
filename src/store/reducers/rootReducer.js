import { combineReducers } from "redux";
import { intlReducer } from "react-intl-redux";
import { reducer as toastrReducer } from "react-redux-toastr";
import { connectRouter } from "connected-react-router";
import appReducer from "./appReducer";

export default history =>
  combineReducers({
    router: connectRouter(history),
    intl: intlReducer,
    toastr: toastrReducer,
    app: appReducer,
  });
