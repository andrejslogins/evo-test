import actionTypes from "../constants/actionTypes";
import produce from "immer";
import { findPreviousIndex } from "../../utils/arrayUtils";

const defaultState = {
  ping: {
    seq: 0,
  },
  tables: {
    selectedTable: null,
    list: [],
    showTables: false,
  },
  authorized: false,
};

/* eslint-disable default-case */
/**
 * Redux reducer for routes
 * @param state
 * @param action
 */
const appReducer = (state = defaultState, action) =>
  produce(state, draft => {
    const { type } = action;
    switch (type) {
      case actionTypes.PING_INCREMENT: {
        draft.ping.seq++;
        break;
      }

      case actionTypes.TABLE_LIST_SET: {
        draft.tables.list = action.payload;
        break;
      }

      case actionTypes.TABLES_TOGGLE: {
        draft.tables.showTables = action.payload;
        break;
      }

      case actionTypes.TABLE_LIST_ADDED: {
        const { afterId, table } = action.payload;
        let idx =
          findPreviousIndex(afterId, draft.tables.list.map(x => x.id)) + 1;
        draft.tables.list.splice(idx, 0, table);
        break;
      }

      case actionTypes.TABLE_LIST_DELETED: {
        const id = action.payload;
        let idx = draft.tables.list.findIndex(x => x.id === id);
        draft.tables.list.splice(idx, 1);
        break;
      }

      case actionTypes.TABLE_LIST_UPDATED: {
        const { id, name, participants } = action.payload;
        let idx = draft.tables.list.findIndex(x => x.id === id);
        draft.tables.list[idx] = {
          id,
          name,
          participants,
        };
        break;
      }

      case actionTypes.AUTHORIZE_SUCCESS: {
        draft.authorized = true;
      }

      case actionTypes.TABLE_EDIT: {
        draft.tables.selectedTable = action.payload;
        break;
      }

      // skip default
    }
  });

export default appReducer;
