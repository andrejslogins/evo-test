import actionTypes from "../../constants/actionTypes";
import reducer from "../appReducer";

describe("table reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual({
      ping: {
        seq: 0,
      },
      tables: {
        list: [],
        showTables: false,
      },
      authorized: false,
    });
  });

  it("should handle TABLE_LIST_SET", () => {
    const action = {
      type: actionTypes.TABLE_LIST_SET,
      payload: [
        {
          id: 1,
          name: "table - James Bond",
          participants: 7,
        },
        {
          id: 2,
          name: "table - Mission Impossible",
          participants: 4,
        },
      ],
    };
    expect(reducer({ tables: { list: [] } }, action)).toEqual({
      tables: {
        list: [
          {
            id: 1,
            name: "table - James Bond",
            participants: 7,
          },
          {
            id: 2,
            name: "table - Mission Impossible",
            participants: 4,
          },
        ],
      },
    });
  });
});
