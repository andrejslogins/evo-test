import actionTypes from "../constants/actionTypes";
import socketTypes from "../constants/socketTypes";
import { toastr } from "react-redux-toastr";

export const sendPing = () => async (dispatch, _, { socket }) => {
  socket.sendJSON({
    $type: socketTypes.SOCKET_PING,
    seq: 1,
  });
};

export const subscribeToTables = () => async (dispatch, _, { socket }) => {
  socket.sendJSON({ $type: socketTypes.SOCKET_TABLE_LIST_SUBSCRIBE });
};

export const unsubscribeFromTables = () => async (dispatch, _, { socket }) => {
  socket.sendJSON({ $type: socketTypes.SOCKET_TABLE_LIST_UNSUBSCRIBE });
};

export const toggleTables = () => async (dispatch, getState) => {
  dispatch({
    type: actionTypes.TABLES_TOGGLE,
    payload: !getState().app.tables.showTables,
  });
};

export const authorize = ({ username, password }) => async (
  dispatch,
  _,
  { socket },
) => {
  socket.sendJSON({
    $type: socketTypes.SOCKET_LOGIN,
    username,
    password,
  });
};

export const incrementPing = () => ({
  type: actionTypes.PING_INCREMENT,
});

export const updateTableList = tables => ({
  type: actionTypes.TABLE_LIST_SET,
  payload: tables,
});

export const notifyNotAuthorized = () => async dispatch => {
  toastr.error("not authorized");
  dispatch({
    type: actionTypes.TABLES_TOGGLE,
    payload: false,
  });
};

export const notifyLoginSuccess = () => async dispatch => {
  toastr.success("logged in");
  dispatch({
    type: actionTypes.AUTHORIZE_SUCCESS,
  });
};

export const notifyLoginFailure = () => async () => {
  toastr.error("login failed");
};

export const notifyTableRemoveFailure = () => async () => {
  toastr.error("table removal failure");
};

export const notifyTableUpdateFailure = () => async () => {
  toastr.error("table update failure");
};

export const addTable = ({ after_id: afterId, table }) => ({
  type: actionTypes.TABLE_LIST_ADDED,
  payload: { afterId, table },
});

export const removeTable = id => ({
  type: actionTypes.TABLE_LIST_DELETED,
  payload: id,
});

export const updateTable = ({ table: { id, name, participants } }) => ({
  type: actionTypes.TABLE_LIST_UPDATED,
  payload: { id, name, participants },
});
export const modifyTable = id => ({
  type: actionTypes.TABLE_EDIT,
  payload: id,
});

export const closeTableModification = () => ({
  type: actionTypes.TABLE_EDIT,
  payload: null,
});

export const emitAddTable = ({ afterId, name, participants }) => async (
  dispatch,
  _,
  { socket },
) => {
  socket.sendJSON({
    $type: socketTypes.SOCKET_TABLE_ADD,
    after_id: afterId,
    table: {
      name,
      participants,
    },
  });
};

export const emitRemoveTable = id => async (dispatch, _, { socket }) => {
  socket.sendJSON({
    $type: socketTypes.SOCKET_TABLE_DELETE,
    id,
  });
};

export const emitUpdateTable = ({ name, participants }) => async (
  dispatch,
  getState,
  { socket },
) => {
  const id = getState().app.tables.selectedTable;
  socket.sendJSON({
    $type: socketTypes.SOCKET_TABLE_UPDATE,
    table: {
      id,
      name,
      participants,
    },
  });
};
