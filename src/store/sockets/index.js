import config from "../../config";
import { store } from "../../index";
import socketTypes from "../constants/socketTypes";
import {
  incrementPing,
  updateTableList,
  notifyNotAuthorized,
  notifyLoginSuccess,
  notifyLoginFailure,
  notifyTableRemoveFailure,
  notifyTableUpdateFailure,
  addTable,
  removeTable,
  updateTable,
} from "../actions/appActions";

// WebSocket.prototype.

const setupSocket = () => {
  const socket = new WebSocket(config.websocket_url);
  socket.sendJSON = msg => {
    socket.send(JSON.stringify(msg));
  };

  socket.onopen = () => {
    console.log("Socket created successfully");
  };

  socket.onmessage = event => {
    const data = JSON.parse(event.data);
    const { $type: type, ...rest } = data;
    switch (type) {
      case socketTypes.SOCKET_PONG: {
        store.dispatch(incrementPing());
        break;
      }
      case socketTypes.SOCKET_TABLE_LIST: {
        store.dispatch(updateTableList(rest.tables));
        break;
      }
      case socketTypes.SOCKET_NOT_AUTHORIZED: {
        store.dispatch(notifyNotAuthorized());
        break;
      }
      case socketTypes.SOCKET_LOGIN_SUCCESS: {
        store.dispatch(notifyLoginSuccess());
        break;
      }
      case socketTypes.SOCKET_LOGIN_FAILURE: {
        store.dispatch(notifyLoginFailure());
        break;
      }
      case socketTypes.SOCKET_TABLE_REMOVE_FAILIRE: {
        store.dispatch(notifyTableRemoveFailure());
        break;
      }
      case socketTypes.SOCKET_TABLE_UPDATE_FAILURE: {
        store.dispatch(notifyTableUpdateFailure());
        break;
      }
      case socketTypes.SOCKET_TABLE_ADDED: {
        store.dispatch(addTable(rest));
        break;
      }
      case socketTypes.SOCKET_TABLE_REMOVED: {
        store.dispatch(removeTable(rest.id));
        break;
      }
      case socketTypes.SOCKET_TABLE_UPDATED: {
        store.dispatch(updateTable(rest));
        break;
      }
      default: {
        break;
      }
    }
  };
  return socket;
};

export default setupSocket;
