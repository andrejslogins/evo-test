import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import setupReducers from "./reducers/rootReducer";
import createHelpers from "./createHelpers";
import { routerMiddleware } from "connected-react-router";
import config from "../config";

/**
 * Redux store export
 * @returns {Store<*, Action> & {dispatch: any}}
 */
export default function configureStore(initialState, history, socket) {
  const helpers = createHelpers({ history, socket });
  const middleware = [thunk.withExtraArgument(helpers)];

  if (config.environment !== "production") {
    middleware.push(require("redux-immutable-state-invariant").default());
    middleware.push(require("redux-logger").createLogger());
  }

  return createStore(
    setupReducers(history),
    initialState,
    applyMiddleware(...middleware, routerMiddleware(history)),
  );
}
