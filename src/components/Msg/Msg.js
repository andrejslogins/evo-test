import React from "react";
import PropTypes from "prop-types";

/**
 * Children are translated
 * @param children
 * @param wrapper
 * @param props
 * @param intl
 * @returns {*}
 * @constructor
 */
const Msg = (
  { children, wrapper: Wrapper, onClick, values = {}, ...props },
  { intl },
) => {
  const translation = intl.formatMessage(
    {
      ...(Wrapper ? { ...props } : {}),
      id:
        children instanceof Array ? children.join().replace(",", "") : children,
    },
    values,
  );
  return Wrapper ? (
    <Wrapper onClick={onClick} {...props}>
      {translation}
    </Wrapper>
  ) : (
    translation
  );
};

Msg.contextTypes = {
  intl: PropTypes.object.isRequired,
};

export default Msg;
