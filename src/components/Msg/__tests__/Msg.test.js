import Msg from "../index";
import React from "react";
import { mountWithIntl } from "../../../helpers/testing";

test("render a translation", () => {
  const msg = mountWithIntl(<Msg>api.fetch.error</Msg>);
  expect(msg).toMatchSnapshot();
});

test("render translation with a wrapper", () => {
  const msg = mountWithIntl(<Msg wrapper={"div"}>api.fetch.error</Msg>);
  expect(msg).toMatchSnapshot();
});
