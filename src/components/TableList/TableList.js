import React, { useEffect } from "react";
import Table from "./components/Table";
import { Grid } from "react-virtualized";
import { Button } from "reactstrap";
import Modal from "../Modal";
import inputTypes from "../Form/inputTypes";
import Form from "../Form";

const TableList = ({
  tables,
  subscribeToTables,
  unsubscribeFromTables,
  width,
  selectedTable,
  closeTableModification,
  modifyTable,
  updateTable,
  addTable,
}) => {
  useEffect(() => {
    subscribeToTables();
    return () => {
      unsubscribeFromTables();
    };
  }, []);

  function cellRenderer({ columnIndex, key, rowIndex, style }) {
    return (
      <div key={key} style={style}>
        <Table {...tables[columnIndex]} />
      </div>
    );
  }

  let form;
  if (selectedTable) {
    if (selectedTable === -1) {
      form = (
        <Modal closeModal={() => closeTableModification()} title={"New Table"}>
          <Form
            submitFunction={addTable}
            fields={[
              {
                names: ["afterId"],
                type: inputTypes.SELECT,
                options: tables.map(({ id: value, name: label }) => ({
                  label,
                  value,
                })),
                labels: ["afterId"],
              },
              {
                names: ["name"],
                type: inputTypes.TEXT,
                labels: ["name"],
              },
              {
                names: ["participants"],
                type: inputTypes.NUMBER,
                labels: ["participants"],
              },
            ]}
          />
        </Modal>
      );
    } else {
      const currentTable = tables.find(x => x.id === selectedTable);
      form = (
        <Modal
          closeModal={() => closeTableModification()}
          title={"Update table"}
        >
          <Form
            submitFunction={updateTable}
            fields={[
              {
                names: ["name"],
                type: inputTypes.TEXT,
                labels: ["name"],
                values: [currentTable.name],
              },
              {
                names: ["participants"],
                type: inputTypes.NUMBER,
                labels: ["participants"],
                values: [currentTable.participants],
              },
            ]}
          />
        </Modal>
      );
    }
  }

  return (
    <>
      <Button
        onClick={() => {
          modifyTable(-1);
        }}
      >
        Add table
      </Button>
      {form}
      <Grid
        cellRenderer={cellRenderer}
        columnCount={tables.length}
        columnWidth={200}
        height={250}
        rowCount={1}
        rowHeight={300}
        width={width}
      />
    </>
  );
};

export default TableList;
