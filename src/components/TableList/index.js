import TableList from "./TableList";
import {
  modifyTable,
  subscribeToTables,
  unsubscribeFromTables,
  closeTableModification,
  emitUpdateTable,
  emitAddTable,
} from "../../store/actions/appActions";
import { connect } from "react-redux";

const mapStateToProps = ({
  app: {
    tables: { list: tables, selectedTable },
  },
}) => ({
  tables,
  selectedTable,
  //Insert redux state parameters if necessarry
});

const mapDispatchToProps = {
  // Insert actions here
  subscribeToTables,
  unsubscribeFromTables,
  closeTableModification,
  modifyTable,
  updateTable: emitUpdateTable,
  addTable: emitAddTable,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TableList);
