import React from "react";
import { Button, Card, CardBody, CardFooter, CardHeader } from "reactstrap";
import config from "../../../../config";
import FontAwesome from "react-fontawesome";

const Table = ({
  participants,
  name,
  style,
  emitRemoveTable,
  modifyTable,
  id,
}) => {
  let participantIcons = [];
  for (let i = 0; i < config.TABLE_MAX_PARTICIPANT_COUNT; i++) {
    const styles = {};
    if (i >= participants) {
      styles.color = "grey";
    }
    participantIcons[i] = <FontAwesome name={"user"} key={i} style={styles} />;
  }

  return (
    <Card style={style}>
      <CardHeader>{name} table</CardHeader>
      <CardBody>{participantIcons}</CardBody>
      <CardFooter>
        <Button
          style={{ width: "50%" }}
          color={"primary"}
          onClick={() => {
            modifyTable(id);
          }}
        >
          Edit
        </Button>
        <Button
          style={{ width: "50%" }}
          color={"danger"}
          onClick={() => {
            emitRemoveTable(id);
          }}
        >
          Delete
        </Button>
      </CardFooter>
    </Card>
  );
};

export default Table;
