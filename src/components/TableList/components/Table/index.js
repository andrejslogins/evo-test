import Table from "./Table";
import { connect } from "react-redux";
import {
  emitRemoveTable,
  modifyTable,
} from "../../../../store/actions/appActions";

const mapDispatchToProps = {
  // Insert actions here
  emitRemoveTable,
  modifyTable,
};

export default connect(
  null,
  mapDispatchToProps,
)(Table);
