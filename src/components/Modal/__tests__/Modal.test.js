import Modal from "../Modal";
import React from "react";

test("render an modal with message passed", () => {
  const component = shallow(<Modal message="api.fetch.error" />);
  expect(component).toMatchSnapshot();
});

test("render a modal without message", () => {
  const component = shallow(<Modal message="" />);
  expect(component).toMatchSnapshot();
});
