import React from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import PropTypes from "prop-types";

const ReactModal = ({ onButtonClick, closeModal, children, title }) => (
  <Modal isOpen={true} toggle={closeModal} backdrop={true}>
    <ModalHeader toggle={onButtonClick}>{title}</ModalHeader>
    <ModalBody>{children}</ModalBody>
  </Modal>
);

ReactModal.propTypes = {
  onButtonClick: PropTypes.func,
};

export default ReactModal;
