import React from "react";
import { Container } from "reactstrap";
import { AppHeader } from "@coreui/react";
import Header from "./components/Header";
import TableActions from "../HeaderActions";

const Layout = ({ children }) => {
  return (
    <div className="app">
      <AppHeader fixed>
        <Header />
      </AppHeader>
      <div className="app-body">
        <main className="main">
          <Container fluid>{children}</Container>
        </main>
      </div>
    </div>
  );
};

export default Layout;
