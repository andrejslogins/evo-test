import Form from "./Form";
import { withFormik } from "formik";
import * as Yup from "yup";
import inputTypes from "./inputTypes";

export default withFormik({
  enableReinitialize: true,
  validateOnBlur: true,
  mapPropsToValues: props => {
    const fields = {};
    props.fields.forEach(
      ({ periodOptions, yearOptions, type, names, values }) => {
        switch (type) {
          case inputTypes.PASSWORD:
          case inputTypes.NUMBER:
          case inputTypes.TEXT: {
            const [name] = names;
            if (values) {
              const [value] = values;
              fields[name] = value.toString();
            } else {
              fields[name] = "";
            }
            break;
          }

          case inputTypes.SELECT: {
            const [name] = names;
            fields[name] = null;
            break;
          }

          default: {
            break;
          }
        }
      },
    );
    return fields;
  },
  handleSubmit: (values, { props }) => {
    const submittedValues = {};
    props.fields.forEach(({ type, names }) => {
      switch (type) {
        case inputTypes.PASSWORD:
        case inputTypes.TEXT: {
          const [name] = names;
          submittedValues[name] = values[name];
          break;
        }

        case inputTypes.NUMBER: {
          const [name] = names;
          submittedValues[name] = Number(values[name]);
          break;
        }

        case inputTypes.SELECT: {
          const [name] = names;
          if (values[name]) {
            submittedValues[name] = values[name].value;
          } else {
            submittedValues[name] = null;
          }
          break;
        }

        default: {
          break;
        }
      }
    });
    props.submitFunction(submittedValues);
  },
  validationSchema: props => {
    const validations = {};
    // Insert validations if necessary
    return Yup.object().shape(validations);
  },
})(Form);
