import React from "react";
import inputTypes from "./inputTypes";
import { Form } from "formik";
import PropTypes from "prop-types";
import Msg from "../Msg";
import { Col, Button, Row } from "reactstrap";
import Input from "./components/Input";
import Select from "./components/Select";

const Filter = (
  { fields, handleSubmit, setFieldValue, setFieldTouched, values, touched },
  { intl },
) => {
  const handleSubmitForm = e => {
    e.preventDefault();
    handleSubmit();
  };

  const filterFields = fields.map(
    ({ type, names, yearOptions, periodOptions, labels, options }, key) => {
      switch (type) {
        case inputTypes.TEXT:
        case inputTypes.NUMBER:
        case inputTypes.PASSWORD: {
          const [name] = names;
          const [label] = labels;
          return (
            <Input
              text={intl.formatMessage({
                id: label,
              })}
              key={key}
              name={name}
              value={values[name]}
              {...type === inputTypes.PASSWORD && { type: "password" }}
            />
          );
        }

        case inputTypes.SELECT: {
          const [name] = names;
          const [label] = labels;
          return (
            <Select
              options={options}
              text={label}
              placeholder={""}
              name={name}
              value={values[name]}
              onChange={setFieldValue}
              onBlur={setFieldTouched}
              touched={touched[name]}
              key={key}
            />
          );
        }

        default: {
          return null;
        }
      }
    },
  );

  return (
    <Form onSubmit={handleSubmitForm} className={"reports-filter"}>
      {filterFields}
      <Row className={"mt25"}>
        <Col md={4} sm={4} lg={3} xl={3}>
          <Button block color={"primary"} type="submit">
            <Msg>submit</Msg>
          </Button>
        </Col>
      </Row>
    </Form>
  );
};

Filter.propTypes = {
  fields: PropTypes.array.isRequired,
};

Filter.contextTypes = {
  intl: PropTypes.object.isRequired,
};

export default Filter;
