import React from "react";
import PropTypes from "prop-types";
import ReactSelect from "react-select";

export default class Select extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    text: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.object.isRequired,
      PropTypes.array.isRequired,
    ]),
    multi: PropTypes.bool,
  };

  handleChange = value => {
    // this is going to call setFieldValue and manually update values.topcis
    this.props.onChange(this.props.name, value);
  };

  handleBlur = () => {
    // this is going to call setFieldTouched and manually update touched.topcis
    this.props.onBlur(this.props.name, true);
  };

  render() {
    const {
      name,
      text,
      value,
      options,
      disabled,
      placeholder,
      isClearable,
      onChange, //unused here but excluded not to override elsewhere used
      onBlur, //unused here but excluded not to override elsewhere used
      ...rest
    } = this.props;

    return (
      <div>
        {text && <label htmlFor={name}>{text}</label>}
        <ReactSelect
          name={name}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={value}
          options={options}
          isDisabled={disabled}
          isClearable={isClearable !== undefined ? isClearable : true}
          placeholder={placeholder ? placeholder : ""}
          {...rest}
        />
      </div>
    );
  }
}
