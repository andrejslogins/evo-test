import React from "react";
import { Label } from "reactstrap";
import { Field } from "formik";
import PropTypes from "prop-types";

/**
 * Bundled error/labels for ease-of-use
 */
const Input = ({ name, text, value, disabled, type, onChange, ...rest }) => {
  return (
    <div>
      <Label for={name}>{text}</Label>
      <Field
        name={name}
        value={value}
        className={"form-control"}
        disabled={disabled}
        type={type || null}
        checked={type === "checkbox" ? value : null}
        {...rest}
      />
    </div>
  );
};

Input.defaultProps = {
  disabled: false,
};

Input.propTypes = {
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  value: PropTypes.string,
  disabled: PropTypes.bool,
};

export default Input;
