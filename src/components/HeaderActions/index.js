import TableActions from "./TableActions";
import connect from "react-redux/es/connect/connect";

const mapStateToProps = ({ app: { authorized } }) => ({
  // Insert state mapping here
  authorized,
});

export default connect(mapStateToProps)(TableActions);
