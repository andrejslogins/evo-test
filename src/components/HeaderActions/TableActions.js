import React from "react";
import Ping from "./components/Ping";
import ToggleTable from "./components/ToggleTable";
import Authorize from "./components/Authorize";
export default ({ authorized }) => (
  <>
    <Ping />
    {!authorized && <Authorize />}
    <ToggleTable />
  </>
);
