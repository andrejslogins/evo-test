import React from "react";
import { Button } from "reactstrap";

const Ping = ({ sendPing, seq }) => (
  <Button onClick={sendPing}>Pings: {seq}</Button>
);

export default Ping;
