import Ping from "./Ping";

import { connect } from "react-redux";
import { sendPing } from "../../../../store/actions/appActions";

const mapStateToProps = state => ({
  //Insert redux state parameters if necessarry
  seq: state.app.ping.seq,
});

const mapDispatchToProps = {
  sendPing,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Ping);
