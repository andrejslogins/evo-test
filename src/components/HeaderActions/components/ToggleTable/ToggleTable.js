import React from "react";
import { Button } from "reactstrap";

export default ({ showTables, toggleTables }) => (
  <Button onClick={toggleTables}>
    {showTables ? "Hide table" : "Show table"}
  </Button>
);
