import ToggleTable from "./ToggleTable";
import { toggleTables } from "../../../../store/actions/appActions";
import { connect } from "react-redux";

const mapStateToProps = ({
  app: {
    tables: { showTables },
  },
}) => ({
  showTables,
  //Insert redux state parameters if necessarry
});

const mapDispatchToProps = {
  // Insert actions here
  toggleTables,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ToggleTable);
