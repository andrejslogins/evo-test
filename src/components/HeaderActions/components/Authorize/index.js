import Authorize from "./Authorize";
import { authorize } from "../../../../store/actions/appActions";
import { connect } from "react-redux";

const mapDispatchToProps = {
  // Insert actions here
  authorize,
};

export default connect(
  null,
  mapDispatchToProps,
)(Authorize);
