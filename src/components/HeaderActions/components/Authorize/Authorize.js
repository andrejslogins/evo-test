import React, { useState } from "react";
import { Button } from "reactstrap";
import Modal from "../../../Modal";
import Form from "../../../Form";
import inputTypes from "../../../Form/inputTypes";
import styles from "./Authorize.module.scss";

export default ({ authorize }) => {
  const [showModal, setShowModal] = useState(false);
  return (
    <>
      <Button onClick={() => setShowModal(true)}>Authorize</Button>
      {showModal && (
        <Modal closeModal={() => setShowModal(false)} title={"Authorization"}>
          <span className={styles.passwordHint}>
            Expected username: user1234
          </span>
          <span className={styles.passwordHint}>
            Expected password: password1234
          </span>
          <Form
            submitFunction={authorize}
            fields={[
              {
                names: ["username"],
                type: inputTypes.TEXT,
                labels: ["username"],
              },
              {
                names: ["password"],
                type: inputTypes.PASSWORD,
                labels: ["password"],
              },
            ]}
          />
        </Modal>
      )}
    </>
  );
};
