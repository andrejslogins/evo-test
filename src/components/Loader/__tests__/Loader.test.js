import Loader from "../index";
import React from "react";
import { mountWithIntl } from "../../../helpers/testing";

test("render an active loader", () => {
  const component = mountWithIntl(<Loader active={true} />);
  expect(component).toMatchSnapshot();
});

test("render an inactive loader", () => {
  const component = mountWithIntl(<Loader active={false} />);
  expect(component).toMatchSnapshot();
});
