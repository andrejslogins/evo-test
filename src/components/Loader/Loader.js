import React from "react";
import ReactLoader from "react-loaders";

export default ({ active, ...rest }) => (
  <ReactLoader active={active} type="ball-spin-fade-loader" {...rest} />
);
