import React from "react";
import { Route as ReactRoute } from "react-router-dom";
import BaseLayout from "../Layout";
import PropTypes from "prop-types";

const Route = ({ loading, component: Component, layout: Layout, ...rest }) => {
  return (
    <ReactRoute
      {...rest}
      render={props => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
};

Route.propTypes = {
  layout: PropTypes.func.isRequired,
  component: PropTypes.object.isRequired,
};

Route.defaultProps = {
  layout: BaseLayout,
};

export default Route;
