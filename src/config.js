export default {
  environment: "development",
  websocket_url: "wss://js-assignment.evolutiongaming.com/ws_api",
  TABLE_MAX_PARTICIPANT_COUNT: 12,
};
